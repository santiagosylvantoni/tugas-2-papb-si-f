package com.example.tugas2_papb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ScondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scond)

        val tvHasil = findViewById<TextView>(R.id.tvHasil)

        val username = intent.getStringExtra("EXTRA_USERNAME")
        val password = intent.getStringExtra("EXTRA_PASSWORD")

        val hasilData = "Username anda adalah $username \n" +
                        "Password anda adalah $password \n"
        tvHasil.text = hasilData
    }
}